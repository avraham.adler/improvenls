# ImproveNLS

Efforts to improve the functioning of the R nls() function for nonlinear least squares estimation. This is part of the Google Summer of Code for 2021.